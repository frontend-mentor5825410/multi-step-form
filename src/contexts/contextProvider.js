import { createContext, useContext, useState } from "react";

const StateContext = createContext({
    viewStep: 1,
    setViewStep: () => {},
})

export const ContextProvider = ({children}) => {

 
    return (
        <StateContext.Provider value={{
            viewStep,
            setViewStep,
        }}>
            {children}
        </StateContext.Provider>
    )
}

export const useStateContext = () => useContext(StateContext);